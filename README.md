# README # 

## Author: Xing Qian, xingq@uoregon.edu ##

This is a solution of the assignment "project5" project for CIS 322

You can view the project description: https://bitbucket.org/UOCIS322/proj5-mongo/src/master/

## Introduction of my solution

The assignment asked for saving data of project 4 by mongoDB, and I have reached this goal.

## Run the project

```
# go to the `DockerMongo` folder, and run the following command.
# at first, we need to build the image and run the image
cd DockerMongo

# start the server
docker-compose up

# test data
# the test would change the data in mongo, so you must run it at first
sh ./tests/tests.sh http://localhost:5000

```

## Details for this assignment

- I tests submit and display button function by testing the api `/submit` and `/display`. The examples of calling the api can be found in `tests\tests.sh`
- User can view brevets form at default and submit the control times.
- If user clicks the display button, it would send Ajax request to `/display`, if there are no data in the mongo, there would do nothing. If there are control times in the mongo, there would open a new page and display all the brevets table.

## API List

### Display

GET http://localhost:5000/display

Response:

If there are no data in mongo:
```
{
  "result":[]
}
```

If there exists data:
```
{
  "result": [
    {
      "begin_time": "2017-01-01T00:00:00+00:00", 
      "brevet_dist_km": 200.0, 
      "control_times": [
        {
          "close": "2017-01-01T06:40:00+00:00", 
          "km": "100", 
          "location": "", 
          "miles": "62.1", 
          "open": "2017-01-01T02:56:00+00:00"
        }
      ]
    }, 
    {
      "begin_time": "2017-01-01T00:00:00+00:00", 
      "brevet_dist_km": 200.0, 
      "control_times": [
        {
          "close": "2017-01-01T06:40:00+00:00", 
          "km": "100", 
          "location": "", 
          "miles": "62.1", 
          "open": "2017-01-01T02:56:00+00:00"
        }, 
        {
          "close": "2017-01-01T13:30:00+00:00", 
          "km": "200", 
          "location": "", 
          "miles": "124.3", 
          "open": "2017-01-01T05:53:00+00:00"
        }
      ]
    }
  ]
}
```

### Submit

POST http://localhost:5000/submit

Post parameters should be like a form. Contains 3 keys: brevet_dist_km, begin_time and control_times.

brevet_dist_km value should be integer. The begin_time value should be time format like 2017-01-01T00:00:00+00:00.
The control_times is an array, each item in it should be a key-value dict. This dict should looks like:
```

```

#### Situation 1: No control times

Request form data:
```
brevet_dist_km: 200
begin_time: 2017-01-01T00:00:00+00:00
control_times: []
```

Response:
```
{
  "error_message": "Control times format should not be empty.", 
  "status": "failed"
}
```

#### Situation 2: Control times open time error

All the control times should be checked by the rules in the url: https://rusa.org/pages/acp-brevet-control-times-calculator

If the data not fit the rule, it would be error, and can not submit data and save it to database.

Request form data:
```
brevet_dist_km: 200
begin_time: 2017-01-01T00:00:00+00:00
control_times: [{"miles":"62.1","km":"100","location":"","open":"2017-01-01T01:56:00+00:00","close":"2017-01-01T06:40:00+00:00"}]
```

Response:
```
{
    "error_message": "Row 1 control time: open time is not correct.",
    "status": "failed"
}
```

#### Situation 3: Control times close time error

Request form data:
```
brevet_dist_km: 200
begin_time: 2017-01-01T00:00:00+00:00
control_times: [{"miles":"62.1","km":"100","location":"","open":"2017-01-01T02:56:00+00:00","close":"2017-01-01T05:40:00+00:00"}]
```

Response:
```
{
    "error_message": "Row 1 control time: close time is not correct.",
    "status": "failed"
}
```

#### Situation 4: Control times submit success

Request form data:
```
brevet_dist_km: 200
begin_time: 2017-01-01T00:00:00+00:00
control_times: [{"miles":"62.1","km":"100","location":"","open":"2017-01-01T02:56:00+00:00","close":"2017-01-01T06:40:00+00:00"}]
```

Response:
```
{
    "status": "success"
}
```