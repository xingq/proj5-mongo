"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevets

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html',
        ACP_ERROR_CONTROL_MORE_THAN_BREVET = acp_times.ACP_ERROR_CONTROL_MORE_THAN_BREVET,
        ACP_ERROR_INVALID_DISTANCE = acp_times.ACP_ERROR_INVALID_DISTANCE,
        ACP_ERROR_INVALID_DATE = acp_times.ACP_ERROR_INVALID_DATE
    )


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_dist_km = request.args.get('brevet_dist_km', 200, type=float)
    begin_date = request.args.get('begin_date')
    begin_time = request.args.get('begin_time')
    app.logger.debug("request.args: {}".format(request.args))

    # and brevets may be longer than 200km
    brevet_start_time = begin_date+'T'+begin_time+':00+00:00'
    open_time = acp_times.open_time(km, brevet_dist_km, brevet_start_time)
    close_time = acp_times.close_time(km, brevet_dist_km, brevet_start_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route("/submit", methods=['POST'])
def submit():
    app.logger.debug("Get post form: {}".format(request.form))
    brevet_dist_km = request.form.get('brevet_dist_km', 0, type=float)
    begin_time = request.form.get('begin_time', '')

    try:
        control_times = flask.json.loads(request.form.get('control_times', ''))
    except:
        return flask.jsonify({"status": "failed", "error_message": "Control times format should be an array."})
    if len(control_times) < 1:
        return flask.jsonify({"status": "failed", "error_message": "Control times format should not be empty."})
    try:
        index = 1
        for control_time in control_times:
            km = float(control_time['km'])
            miles = float(control_time['miles'])
            open = control_time['open']
            close = control_time['close']

            if km <= 0 or miles <= 0:
                message = "Row {} control time: km or miles is not positive number.".format(index)
                return flask.jsonify({"status": "failed", "error_message": message})

            if round(km, 0) != round(miles*1.609344, 0):
                message = "Row {} control time: km and miles are not correct. input km: {}, expected: {} ".format(index, km, round(miles*1.609344, 1))
                return flask.jsonify({"status": "failed", "error_message": message})
            
            if open != acp_times.open_time(km, brevet_dist_km, begin_time):
                message = "Row {} control time: open time is not correct.".format(index)
                return flask.jsonify({"status": "failed", "error_message": message})
            
            if close != acp_times.close_time(km, brevet_dist_km, begin_time):
                message = "Row {} control time: close time is not correct.".format(index)
                return flask.jsonify({"status": "failed", "error_message": message})

            index += 1
    except:
        return flask.jsonify({"status": "failed", "error_message": "Control times format error."})

    item_doc = {
        'brevet_dist_km': brevet_dist_km,
        'begin_time': begin_time,
        'control_times': control_times
    }
    db.brevets.insert_one(item_doc)

    result = {"status": "success"}
    return flask.jsonify(result)

@app.route('/show')
def show():
    return flask.render_template('display.html')

@app.route('/display')
def display():
    _items = db.brevets.find()

    items = []
    for item in _items:
        del item['_id']
        items.append(item)

    app.logger.debug(items)

    return flask.jsonify(result=items)
#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)

